;=================================
;File: code_9_1
;=================================

.code
	LDA var1         ;este bloco realiza: var1 = var1 + 1.
	ADD one
	STA var1

end:
	INT exit

.data
	;syscall exit
	exit: DD 25
	var1: DD 10      ;int var1 = 10.
	one: DD 1        ;int one = 1.

.stack 1
